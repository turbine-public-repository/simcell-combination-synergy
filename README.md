# README #

This repository collects all code needed for the evaluation steps and performance analysis of Papp and Jordan et al. Network-driven cancer cell avatars for combination discovery and biomarker identification for DNA Damage Response inhibitors.


__Folder structure:__

_Python_files_

Contains the Python scripts which are 1) discretizing synergy values with k-means clustering and 2) visualizing the benchmarking of Turbine results to Dream challenge results.

_R_files_

Contains the necessary R files for the visualizations including stacked bar plots, ROC curves and statistical hypothesis tests.  

_data_

Processed and raw input data for the scripts. The folder consists of accuracy tables, post-process combination biomarker data, MoA categories, combination metrics, monotherapy responses, in silico - in vitro table etc. 
Noteworthy, in the case of TCGA input the required data were downloaded from cBioPortal (https://www.cbioportal.org/datasets) and are not uploaded to this repository. 

_results_

Contains all figures and p-values computed by the scripts of _Python_files_ and _R_files_ folder.

_src_

Necessary R functions for statistical interpretations. 

