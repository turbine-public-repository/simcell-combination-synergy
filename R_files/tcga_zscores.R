library("data.table")
library("tidyverse")

# export path
export_path = "../results/"

# path to combiomarker gene list
bm_path = "../data/processed/expression_markers.txt"

# cBioPortal TCGA data
tcga_folders = list.dirs(path = "../data/TCGA_PanCan_2018-cBioportal/",
                         full.names = TRUE,
                         recursive = FALSE)
tcga_folders = tcga_folders[grepl(tcga_folders, pattern = "_tcga_pan_can_atlas_2018")]

# prioritized TCGA tumor types
tumors = c(
  "BLCA",
  "LAML",
  "LCML",
  "BRCA",
  "KIRC",
  "KIRP",
  "COAD",
  "READ",
  "COADREAD",
  "KIRC",
  "KIRP",
  "NSCLC",
  "LUAD",
  "LUSC",
  "OV",
  "PRAD",
  "SKCM",
  "STAD")

# combiomarker gene list
bm = fread(bm_path, header = FALSE)
bm = bm$V1

# determine those patients who express the given biomarker altered than the other patients
# about z scores: Log-transformed mRNA expression z-scores compared to the expression distribution of all samples (RNA Seq V2 RSEM)
diff_expression = function(biomarker,
                           tcga_folder,
                           z_score_thr = 2) {
  
  # create TCGA label (the strsplit depends on mutation data path!)
  tcga_project = str_to_upper(strsplit(tcga_folder, "//")[[1]][2])
  tcga_project = str_split(tcga_project, "_")[[1]][1]
  
  # number of patients
  clinical = fread(paste0(tcga_folder, "/data_clinical_patient.txt"))
  clinical = clinical[5:nrow(clinical), ]
  n_of_patients = length(unique(clinical$`#Patient Identifier`))
  
  zs = fread(paste0(tcga_folder, "/data_RNA_Seq_v2_mRNA_median_all_sample_Zscores.txt"))
  zs = zs %>% filter(Hugo_Symbol == biomarker)
  start = colnames(zs)[3]
  end = colnames(zs)[ncol(zs)]
  zs = zs %>% pivot_longer(.,
                           names_to = "patient_barcode",
                           values_to = "z_score",
                           cols = start:end) %>%
    select(biomarker = Hugo_Symbol, patient_barcode, z_score)
  zs_diff = zs %>% filter(z_score >= z_score_thr | z_score <= z_score*(-1))
  n_of_diffexpr = length(unique(zs_diff$patient_barcode))
  n_of_underexpr = zs_diff %>% filter(z_score < 0) %>% pull(z_score) %>% length
  n_of_overexpr = zs_diff %>% filter(z_score > 0) %>% pull(z_score) %>% length
  
  output = tibble(
    tcga_project = tcga_project,
    gene_symbol = biomarker,
    n_of_diffexpr = n_of_diffexpr,
    n_of_overexpr = n_of_overexpr,
    n_of_underexpr = n_of_underexpr,
    n_of_patients = n_of_patients
  )
  
  return(output)
  
}

# collecting
diffs = list()
for (i in 1:length(tcga_folders)) {
  diffs[[i]] = lapply(
    bm,
    diff_expression,
    tcga_folder = tcga_folders[i]
  )
}
diffs = bind_rows(diffs)
diffs = diffs %>% arrange(desc(n_of_diffexpr)) %>% filter(tcga_project %in% tumors)

# export
write_tsv(diffs, paste0(export_path, "combiomarker-TCGA_zscore_analysis.tsv"))
